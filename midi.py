from mido import Message, MidiTrack, MidiFile
import chord

BEAT = 32

def create_midi_file():
    midi_file = MidiFile()
    midi_file.ticks_per_beat = BEAT
    track = MidiTrack()
    midi_file.tracks.append(track)
    return midi_file, track

def add_chord(track, chord, length):
    # note on
    track.append(Message('note_on', note=chord.root, time=0))
    track.append(Message('note_on', note=chord.third, time=0))
    track.append(Message('note_on', note=chord.fifth, time=0))
    # note off
    track.append(Message('note_off', note=chord.root, time=length*BEAT))
    track.append(Message('note_off', note=chord.third, time=0))
    track.append(Message('note_off', note=chord.fifth, time=0))

