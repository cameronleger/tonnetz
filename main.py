# TODO
# random walker

import argparse
import re

import chord
import midi
import transformer

def get_chord(chord_name):
    pattern = r"([A-G]+b?\d+)([Mm])"
    if not re.match(pattern, chord_name):
        raise RuntimeError("Can't parse chord name!")
    note_name, quality = re.match(pattern, chord_name).groups()
    if quality == 'm':
        return chord.MinorTriad(note_name)
    elif quality == 'M':
        return chord.MajorTriad(note_name)

class Note(object):
    def __init__(self, note):
        try:
            self.note = int(note)
        except:
            self.note = chord.note_to_number(note)
    def __repr__(self):
        return f"{self.note} - {chord.number_to_note(self.note)}"

parser = argparse.ArgumentParser(
    epilog="The arguments to --low and --high can be given as MIDI note numbers from "
        "0-127, or as note names with the octave specified as in the starting_chord "
        "argument: C4, Eb3, etc.")
parser.add_argument(
    "starting_chord",
    help="The chord to begin the transformation with. "
        "Note name must be upper case. "
        "Note must be natural or flat (i.e. not sharp) and use M for major or m for minor. "
        "Must contain the octave number (middle C is C4). "
        "Don't try to be clever and use Cb or other enharmonic equivalents. "
        "For example: C4M = C major with middle C root, Eb3m = E flat minor."
    )
parser.add_argument(
    "transformation",
    help="The transformations to apply to the starting chord. "
        "These are defined in terms of P, L and R transformations. "
        "You can apply several transformations at once. "
        "Between each set of transformations you must specify how long the chord "
        "should last in beats. "
        "For example: 'p2lr4plr8' does a P transformation for two beats, an LR transformation "
        "for four beats, then a PLR transformation for eight beats."
    )
parser.add_argument(
    "--low", type=Note, default=Note(0),
    help="Constrain the lowest note output to this value. Can be given as a note name or a number.")
parser.add_argument(
    "--high", type=Note, default=Note(127),
    help="Constrain the highest note output to this value. Can be given as a note name or a number.")
parser.add_argument(
    "--repeat", type=int, default=1, help="Repeat the input pattern this number of times")
parser.add_argument(
    "--midi-path", help="filename to output midi data to, default 'chords.mid'", default="chords.mid")
args = parser.parse_args()

starting_chord = get_chord(args.starting_chord)
transformation = args.transformation * args.repeat
chords = transformer.apply_transformation(starting_chord, transformation)
midi_file, midi_track = midi.create_midi_file()

# begin with the starting chord for a bar
midi.add_chord(midi_track, starting_chord.constrain(args.low.note, args.high.note), 4)
for triad, length in chords:
    midi.add_chord(midi_track, triad.constrain(args.low.note, args.high.note), length)
midi_file.save(args.midi_path)
