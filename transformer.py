''' 
Parse the input command for Riemannian transformations.
It looks like this:

    p4l2pr4lpr8

Some letters, either p, l or r for parallel, leading, relative
and a number defining how many beats the chord should last.
'''

import chord
import re
import unittest

input_pattern = r"([plr]+\d+)"
transformation_pattern = r"([plr]+)(\d+)"

def get_chord_transformations(input_string):
    if not re.match(input_pattern, input_string):
        raise InputError("Can't parse input pattern!")
    return re.findall(input_pattern, input_string)

def apply_transformation(chord, input_string):
    transformed_chord = chord
    chord_transformations = get_chord_transformations(input_string)
    transformed_chords = []
    for chord_transformation in chord_transformations:
        parts = re.findall(transformation_pattern, chord_transformation)[0]
        transformation, length = parts[0], int(parts[1])
        for move in transformation:
            if move == 'l':
                transformed_chord = transformed_chord.L()
            elif move == 'r':
                transformed_chord = transformed_chord.R()
            elif move == 'p':
                transformed_chord = transformed_chord.P()
        transformed_chords.append((transformed_chord, length))
    return transformed_chords


class TestTransformer(unittest.TestCase):

    def convert_and_assert(self, input_chord, input_string, expected):
        transformed_chords = apply_transformation(input_chord, input_string)
        for i, (expected_chord, expected_length) in enumerate(expected):
            transformed_chord, length = transformed_chords[i]
            self.assertEqual(transformed_chord.root, expected_chord.root)
            self.assertEqual(transformed_chord.third, expected_chord.third)
            self.assertEqual(transformed_chord.fifth, expected_chord.fifth)
            self.assertEqual(length, expected_length)

    def test_first_order_transformation(self):
        input_chord = chord.MajorTriad('C4', 'E4', 'G4')
        expected_chord = chord.MinorTriad('C4', 'Eb4', 'G4')
        input_string = 'p4'
        self.convert_and_assert(input_chord, input_string, [(expected_chord, 4)])

    def test_second_order_transformation(self):
        input_chord = chord.MajorTriad('C4', 'E4', 'G4')
        # in the PR transformation, we get an Eb major chord in 2nd inversion
        expected_chord = chord.MajorTriad('Eb4', 'G4', 'Bb3')
        input_string = 'pr16'
        self.convert_and_assert(input_chord, input_string, [(expected_chord, 16)])

    def test_third_order_transformation(self):
        input_chord = chord.MajorTriad('C4', 'E4', 'G4')
        # in the PRL transformation, we get a G minor chord in 1st inversion
        expected_chord = chord.MajorTriad('G4', 'Bb3', 'D4')
        input_string = 'prl2'
        self.convert_and_assert(input_chord, input_string, [(expected_chord, 2)])

    def test_two_first_order_transformations(self):
        input_chord = chord.MinorTriad('C3', 'Eb3', 'G3')
        # P gives us the parallel major
        expected_chord_one = chord.MajorTriad('C3', 'E3', 'G3')
        # R gives is the relative minor in 1st inversion
        expected_chord_two = chord.MinorTriad('A3', 'C3', 'E3')
        input_string = 'p2r4'
        self.convert_and_assert(
            input_chord,
            input_string,
            [(expected_chord_one, 2), (expected_chord_two, 4)]
            )

if __name__ == '__main__':
    unittest.main()
