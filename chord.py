import math
import unittest

notes_flat = ['C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B']
notes_sharp = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']

MIDDLE_C = 60 # according to the MIDI spec


def note_index(note_number):
    return (note_number - MIDDLE_C) % 12

def number_to_note(note_number):
    octave_number = math.floor(note_number/12)-1
    note_letter = notes_flat[note_index(note_number)]
    note_name = f"{note_letter}{octave_number}"
    return note_name

_note_to_number = {}
for i in range(127):
    _note_to_number[number_to_note(i)] = i

def note_to_number(note_name):
    return _note_to_number[note_name]

def notes_within_range(note, low, high):
    notes_within_range = []
    index = note_index(note)
    low_instance = -(12*2) + index
    number_octaves = 12
    note_instances = [low_instance+(12*i) for i in range(number_octaves)]
    return [x for x in note_instances if x <= high and x >= low]

def constrain_note_to_range(note, low, high):
    if (high - low) + 1 < 12:
        raise RuntimeError("Can't use a range lower than one octave")
    if low <= note and high >= note:
        return note
    notes_in_range = notes_within_range(note, low, high)
    if note < low:
        return notes_in_range[0]
    if note > high:
        return notes_in_range[-1]


class Triad(object):
    ''' Represents a triad in any inversion '''
    def __init__(self, root, third, fifth):
        for note in [('root', root), ('third', third), ('fifth', fifth)]:
            name, value = note
            if type(value) == str:
                self.__dict__[name] = note_to_number(value)
            elif type(value) == int:
                self.__dict__[name] = value

    def __repr__(self):
        return (
        f'{self.root}: {number_to_note(self.root)} | '
        f'{self.third}: {number_to_note(self.third)} | '
        f'{self.fifth}: {number_to_note(self.fifth)}')

    def constrain(self, low, high):
        ''' Return a version of the chord constrained to the given note range. '''
        return Triad(
            constrain_note_to_range(self.root, low, high),
            constrain_note_to_range(self.third, low, high),
            constrain_note_to_range(self.fifth, low, high)
            )

    def parallel(self):
        raise NotImplementedError()

    def relative(self):
        raise NotImplementedError()

    def leading(self):
        raise NotImplementedError()

    def P(self):
        return self.parallel()

    def R(self):
        return self.relative()

    def L(self):
        return self.leading()


class MajorTriad(Triad):
    def __init__(self, root, third=None, fifth=None):
        if third is None or fifth is None:
            if type(root) == str:
                root = note_to_number(root)
            third = root + 4
            fifth = root + 7
        super().__init__(root, third, fifth)

    def parallel(self):
        return MinorTriad(self.root, self.third-1, self.fifth)

    def relative(self):
        return MinorTriad(self.fifth+2, self.root, self.third)

    def leading(self):
        return MinorTriad(self.third, self.fifth, self.root-1)

    def constrain(self, low, high):
        base_triad = super().constrain(low, high)
        return MajorTriad(base_triad.root, base_triad.third, base_triad.fifth)


class MinorTriad(Triad):
    def __init__(self, root, third=None, fifth=None):
        if third is None or fifth is None:
            if type(root) == str:
                root = note_to_number(root)
            third = root + 3
            fifth = root + 7
        super().__init__(root, third, fifth)

    def parallel(self):
        return MajorTriad(self.root, self.third+1, self.fifth)

    def relative(self):
        return MajorTriad(self.third, self.fifth, self.root-2)

    def leading(self):
        return MajorTriad(self.fifth+1, self.root, self.third)

    def constrain(self, low, high):
        base_triad = super().constrain(low, high)
        return MinorTriad(base_triad.root, base_triad.third, base_triad.fifth)



class TestChordFunctions(unittest.TestCase):

    def test_creating_a_triad_with_numbers_works(self):
        triad = Triad(60, 64, 67)
        self.assertEqual(triad.root, 60)
        self.assertEqual(triad.third, 64)
        self.assertEqual(triad.fifth, 67)

    def test_creating_a_triad_with_note_names(self):
        triad = Triad('C4', 'E4', 'G4')
        self.assertEqual(triad.root, 60)
        self.assertEqual(triad.third, 64)
        self.assertEqual(triad.fifth, 67)

    def test_constrain_within_range_returns_same_note(self):
        self.assertEqual(constrain_note_to_range(MIDDLE_C, 0, 100), 60)

    def test_constrain_tight_range_returns_same_note(self):
        self.assertEqual(constrain_note_to_range(MIDDLE_C, MIDDLE_C, MIDDLE_C + 12), MIDDLE_C)

    def test_constrain_note_below_single_octave_range(self):
        c_above_middle_c = MIDDLE_C + 12
        self.assertEqual(
            constrain_note_to_range(MIDDLE_C, MIDDLE_C + 4, MIDDLE_C + 12 + 4), c_above_middle_c)

    def test_constrain_note_above_single_octave_range(self):
        f_above_middle_c = MIDDLE_C + 5 # 65
        f_two_above_middle_c = MIDDLE_C + 5 + 12 # 77
        low = MIDDLE_C
        high = MIDDLE_C + 11 # B above middle C
        self.assertEqual(
            constrain_note_to_range(f_two_above_middle_c, low, high), f_above_middle_c)

    def test_constrain_note_above_single_offset_octave_range(self):
        f_above_middle_c = MIDDLE_C + 5 # 65
        f_two_above_middle_c = MIDDLE_C + 5 + 12 # 77
        low = MIDDLE_C + 2 # D above middle C, 62
        high = low + 11 # Db two above middle C, 73
        self.assertEqual(
            constrain_note_to_range(f_two_above_middle_c, low, high), f_above_middle_c)

    def test_constrain_note_below_two_octave_range(self):
        c_above_middle_c = MIDDLE_C + 12
        d_above_middle_c = MIDDLE_C + 2
        d_three_above_middle_c = d_above_middle_c + 24
        self.assertEqual(
            constrain_note_to_range(MIDDLE_C, d_above_middle_c, d_three_above_middle_c), c_above_middle_c)

    def test_constrain_note_above_two_octave_range(self):
        c_two_above_middle_c = MIDDLE_C + (12*2) # 84
        c_four_above_middle_c = MIDDLE_C + (12*4) # 96
        low = MIDDLE_C + 2 # D above middle C, 62
        high = low + 23 # Db three above middle C, 85
        self.assertEqual(
            constrain_note_to_range(c_four_above_middle_c, low, high), c_two_above_middle_c)


if __name__ == '__main__':
    unittest.main()
